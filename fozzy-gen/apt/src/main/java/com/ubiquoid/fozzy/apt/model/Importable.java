package com.ubiquoid.fozzy.apt.model;

import java.util.Set;

public interface Importable {

	Set<String> getImports();
}
