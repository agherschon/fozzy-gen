package com.ubiquoid.fozzy.apt.processor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;

import com.ubiquoid.fozzy.api.annotation.Model;
import com.ubiquoid.fozzy.api.annotation.WebServiceHelper;
import com.ubiquoid.fozzy.api.annotation.WebServiceMethod;
import com.ubiquoid.fozzy.apt.model.ClassModelName;
import com.ubiquoid.fozzy.apt.model.Helper;
import com.ubiquoid.fozzy.apt.model.Method;
import com.ubiquoid.fozzy.apt.model.ParameterTypeName;
import com.ubiquoid.fozzy.apt.util.ProcessorLogger;
import com.ubiquoid.fozzy.apt.util.StringUtils;
import com.ubiquoid.fozzy.apt.util.TypeUtils;

import freemarker.cache.ClassTemplateLoader;
import freemarker.template.Configuration;

@SupportedAnnotationTypes({ "com.ubiquoid.fozzy.api.annotation.Model", "com.ubiquoid.fozzy.api.annotation.WebServiceHelper", "com.ubiquoid.fozzy.api.annotation.WebServiceMethod" })
@SupportedSourceVersion(SourceVersion.RELEASE_6)
public class FozzyProcessor extends AbstractProcessor {

	private Configuration cfg = new Configuration();
	private ProcessorLogger logger;

	private List<Helper> helpers = new ArrayList<Helper>();
	private HashMap<String, List<Method>> models = new HashMap<String, List<Method>>();

	@Override
	public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {

		this.cfg.setTemplateLoader(new ClassTemplateLoader(this.getClass(), "/res"));
		this.logger = new ProcessorLogger(processingEnv.getMessager());
		logger.info("Running FozzyProcessor...");

		if (annotations.size() < 1) {
			return true;
		}

		for (Element element : roundEnv.getElementsAnnotatedWith(WebServiceHelper.class)) {

			WebServiceHelper annotation = element.getAnnotation(WebServiceHelper.class);

			if (element.getKind() == ElementKind.INTERFACE) {

				TypeElement typeElement = (TypeElement) element;

                logger.info("typeElement = " + typeElement);
                logger.info("typeElement package = " + StringUtils.getPackageName(typeElement.getQualifiedName().toString()));


				Helper helper = new Helper();
				helper.setImplementedClassModelName(new ClassModelName(typeElement.getQualifiedName().toString()));
				helper.setClassModelName(new ClassModelName(typeElement.getQualifiedName().toString(), annotation.name()));
				
				//helper.setHelperPackageName(annotation.helperPackage());
				//helper.setParserPackageName(annotation.parserPackage());
				

				for (Element childElement : typeElement.getEnclosedElements()) {

					if (childElement.getKind() == ElementKind.METHOD) {

						WebServiceMethod methodAnnotation = childElement.getAnnotation(WebServiceMethod.class);

						Method method = new Method();
						method.setUrl(methodAnnotation.url());
						ExecutableElement executableElement = (ExecutableElement) childElement;
						method.setName(executableElement.getSimpleName().toString());
						method.setReturnType(TypeUtils.getTypeName(executableElement.getReturnType()));
						for (VariableElement variableElement : executableElement.getParameters()) {
							method.getParameters().add(new ParameterTypeName(TypeUtils.getTypeName(variableElement.asType()), variableElement.toString()));
						}
						helper.getMethods().add(method);
					}
				}
				helpers.add(helper);
			}
		}


		for (Element element : roundEnv.getElementsAnnotatedWith(Model.class)) {

			if (element.getKind() == ElementKind.CLASS) {

				TypeElement typeElement = (TypeElement) element;

				List<Method> methods = new ArrayList<Method>();

				for (Element childElement : typeElement.getEnclosedElements()) {

					if (childElement.getKind() == ElementKind.METHOD && childElement.getSimpleName().toString().startsWith("set")) {

						ExecutableElement executableElement = (ExecutableElement) childElement;
						Method method = new Method();
						method.setName(executableElement.getSimpleName().toString());
						method.setReturnType(TypeUtils.getTypeName(executableElement.getReturnType()));
						for (VariableElement variableElement : executableElement.getParameters()) {

							method.getParameters().add(new ParameterTypeName(TypeUtils.getTypeName(variableElement.asType()), variableElement.toString()));
						}
						methods.add(method);
					}
				}

				models.put(typeElement.toString(), methods);
			}
		}
		logger.info("models = " + models);


		/*
		for (Helper helper : helpers) {
			
			for (Method method : helper.getMethods()) {

				Parser parser = new Parser();
				parser.setGenericType(method.getReturnType());
				parser.setClassModelName(new ClassModelName(helper.getParserPackageName(), StringUtils.getParserName(method.getName())));
				TemplateGenerator.generateParser(logger, processingEnv, cfg, parser);
				// adds the parser reference to the method to be able to use it in the Helper
				method.setParser(parser);
			}
			
			TemplateGenerator.generateHelper(logger, processingEnv, cfg, helper);
		}
		*/
		return true;
	}
}
