
package com.ubiquoid.fozzy.app.helper;

import com.ubiquoid.fozzy.api.annotation.DateFormat;
import com.ubiquoid.fozzy.api.annotation.WebServiceHelper;
import com.ubiquoid.fozzy.api.annotation.WebServiceMethod;
import com.ubiquoid.fozzy.app.model.Post;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

@WebServiceHelper(name="PostWebServicHelper")
public interface IPostWebServiceHelper {

	@WebServiceMethod(url="http://garage.ubiquoid.com/postId.json.php")
    int getPostId();
	
	@WebServiceMethod(url="http://garage.ubiquoid.com/post.json.php")
    Post getPost();

	@WebServiceMethod(url="http://garage.ubiquoid.com/posts.json.php")
    ArrayList<Post> getPosts();
	
	@WebServiceMethod(url="http://garage.ubiquoid.com/posts.json.php?date=$s")
    HashMap<Integer, HashMap<Object, Post>> getPostsByDate(@DateFormat(format = "d/m/Y") Date date, int delta);
}
